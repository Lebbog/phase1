<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html_Swagger UI</name>
   <tag></tag>
   <elementGuidId>c6cd8540-83e9-4673-83bb-4859f0c9608a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  
  Swagger UI
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  
  
  
  

  
    $(function () {
      var url = window.location.search.match(/url=([^&amp;]+)/);
      if (url &amp;&amp; url.length > 1) {
        url = decodeURIComponent(url[1]);
      } else {
        url = &quot;http://petstore.swagger.io/v2/swagger.json&quot;;
      }

      // Get Swashbuckle config into JavaScript
      function arrayFrom(configString) {
        return (configString !== &quot;&quot;) ? configString.split('|') : [];
      }

      function stringOrNullFrom(configString) {
        return (configString !== &quot;null&quot;) ? configString : null;
      }

      window.swashbuckleConfig = {
        rootUrl: 'http://sds-katalon.azurewebsites.net:80',
        discoveryPaths: arrayFrom('swagger/docs/v1'),
        booleanValues: arrayFrom('true|false'),
        validatorUrl: stringOrNullFrom(''),
        customScripts: arrayFrom(''),
        docExpansion: 'none',
        oAuth2Enabled: ('false' == 'true'),
        oAuth2ClientId: '',
        oAuth2ClientSecret: '',
        oAuth2Realm: '',
        oAuth2AppName: '',
        oAuth2ScopeSeperator: ' ',
        oAuth2AdditionalQueryStringParams: JSON.parse('{}')
      };

      // Pre load translate...
      if(window.SwaggerTranslator) {
        window.SwaggerTranslator.translate();
      }
      window.swaggerUi = new SwaggerUi({
        url: swashbuckleConfig.rootUrl + &quot;/&quot; + swashbuckleConfig.discoveryPaths[0],
        dom_id: &quot;swagger-ui-container&quot;,
        booleanValues: swashbuckleConfig.booleanValues,
        onComplete: function(swaggerApi, swaggerUi){
          if (typeof initOAuth == &quot;function&quot; &amp;&amp; swashbuckleConfig.oAuth2Enabled) {
            initOAuth({
              clientId: swashbuckleConfig.oAuth2ClientId,
              clientSecret: swashbuckleConfig.oAuth2ClientSecret,
              realm: swashbuckleConfig.oAuth2Realm,
              appName: swashbuckleConfig.oAuth2AppName,
              scopeSeparator: swashbuckleConfig.oAuth2ScopeSeperator,
              additionalQueryStringParams: swashbuckleConfig.oAuth2AdditionalQueryStringParams
            });
          }

          if(window.SwaggerTranslator) {
            window.SwaggerTranslator.translate();
          }

          $('pre code').each(function(i, e) {
            hljs.highlightBlock(e)
          });

          addApiKeyAuthorization();

          window.swaggerApi = swaggerApi;
          _.each(swashbuckleConfig.customScripts, function (script) {
            $.getScript(script);
          });
        },
        onFailure: function(data) {
          log(&quot;Unable to Load SwaggerUI&quot;);
        },
        docExpansion: swashbuckleConfig.docExpansion,
        jsonEditor: false,
        apisSorter: null, // default to server
        defaultModelRendering: 'schema',
        showRequestHeaders: false
      });

      if (window.swashbuckleConfig.validatorUrl !== '')
        window.swaggerUi.options.validatorUrl = window.swashbuckleConfig.validatorUrl;

      function addApiKeyAuthorization(){
        var key = encodeURIComponent($('#input_apiKey')[0].value);
        if (key &amp;&amp; key.trim() != &quot;&quot;) {
          var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization(&quot;api_key&quot;, key, &quot;query&quot;);
            window.swaggerUi.api.clientAuthorizations.add(&quot;api_key&quot;, apiKeyAuth);
            log(&quot;added key &quot; + key);
        }
      }

      $('#input_apiKey').change(addApiKeyAuthorization);

      // if you have an apiKey you would like to pre-populate on the page for demonstration purposes...
      /*
        var apiKey = &quot;myApiKeyXXXX123456789&quot;;
        $('#input_apiKey').val(apiKey);
      */

      window.swaggerUi.load();

      function log() {
        if ('console' in window) {
          console.log.apply(console, arguments);
        }
      }
  });
  
#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-elementInfoDiv {color: lightblue; padding: 5px}



  
    swagger
    
      
      
      Explore
    
  




  SynectDataServer
  
  
  
  
  
  


  
  
    CnarioMonitoringApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/CnarioMonitoringApi/Get
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          apart



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    CommonUtilsApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/SetLoggerLevel/{level}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          level

  



       Verbose  


       Debug  


       Information  


       Warning  


       Error  


       Fatal  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetTime
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetDateTime
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetAirlinesAutoComplete/{search}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          search



	

path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetAirportsAutoComplete/{search}/{limitResults}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          search



	

path
string
limitResults



	

path
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetServerStatus
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetRedundancyMode
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/HasPendingErrors
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetFileUpdateTimes
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetConfigFilesMetadata
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetAirlineName
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineCode



	

query
string
codeType



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/GetInMemoryAirlinesNamesAndMcoCodes
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CommonUtilsApi/SendEmail
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          client



	

query
string
subject



	

query
string
body



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    CounterTitlesApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/CounterTitlesModule/GetSetPins
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CounterTitlesModule/GetLatestSetPinsUpdateTime
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/CounterTitlesModule/RunPinsValidation/{quadName}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    FilesApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/FilesApi/GetFile
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          fileName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/FilesApi/UpdateFile
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          updateModel

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    FileUpdateModel {FileName (string, optional),Content (string, optional),ModelType (string, optional),PassPhrase (string, optional, read only)}
  

  
    {
  &quot;FileName&quot;: &quot;string&quot;,
  &quot;Content&quot;: &quot;string&quot;,
  &quot;ModelType&quot;: &quot;string&quot;,
  &quot;PassPhrase&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/FilesApi/UpdateCsvFile
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          updateModel

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    FileUpdateModel {FileName (string, optional),Content (string, optional),ModelType (string, optional),PassPhrase (string, optional, read only)}
  

  
    {
  &quot;FileName&quot;: &quot;string&quot;,
  &quot;Content&quot;: &quot;string&quot;,
  &quot;ModelType&quot;: &quot;string&quot;,
  &quot;PassPhrase&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    GoaaFidApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetDbLastUpdateTime
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetAllFlights/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetXNextFlightsForAirline/{airlineDetails}/{next}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineDetails



	

path
string
next



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetXNextFlightsOfAirlineXWithCutoff/{airlineDetails}/{next}/{cutoffHour}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineDetails



	

path
string
next



	

path
integer
cutoffHour



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetFlight/{airlineDetails}/{flightNumber}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineDetails



	

path
string
flightNumber



	

path
string
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetAllFlightsForTheNextXHours/{xHours}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xHours



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetAllFlightsForTheNextXDays/{xDays}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xDays



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetAllFlightsForTheLastXHours/{xHours}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xHours



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetAllFlightsForTheLastXDays/{xDays}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xDays



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/GoaaFid/GetXFlightsForAirlineFromUntilOffset/{minutesBefore}/{minutesAfter}/{airlineDetails}/{count}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          minutesBefore



	

path
integer
minutesAfter



	

path
integer
airlineDetails



	

path
string
count



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    HangfireCleanupApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/HangfireCleanupApi/ForceCleanup
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    HeartbeatApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/HeartbeatApi/Get
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/HeartbeatApi/GetRedundancyLogs
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          count



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/HeartbeatApi/GetConfigFilesMd5
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    ImageBankApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /ImageBank/GetFilePath
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineCode



	

query
string
compName



	

query
string
unitNumber



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /ImageBank/UploadBinaryImage
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          model

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    BinaryImageModel {AirlineCode (string, optional),CompName (string, optional),UnitNumber (integer, optional),Data (string, optional)}
  

  
    {
  &quot;AirlineCode&quot;: &quot;string&quot;,
  &quot;CompName&quot;: &quot;string&quot;,
  &quot;UnitNumber&quot;: 0,
  &quot;Data&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /ImageBank/HasImage
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineCode



	

query
string
compName



	

query
string
unitNumber



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /ImageBank/TransactionStart
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /ImageBank/TransactionEnd
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    MonitorFailoverApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/SendEndOfTimesImageDownloadsOfAllMonitors
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/SendEndOfTimesImageDownloadsOfSpecifiedQuad
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/SendEndOfTimesImageDownloadsOfSpecifiedMonitor
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitorIndex



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/SendEndOfTimesImageDownloadsWithCustomImageNames
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          customNames



	

query
string
resetCurrentRepository

  



       true  


       false  


  



query
boolean

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/ResetEndOfTimesImageRepositoryOfAllMonitors
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/ResetEndOfTimesImageRepositoryOfSpecifiedQuad
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverModule/ResetEndOfTimesImageRepositoryOfSpecifiedMonitor
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitorIndex



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOn
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitors



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOnByAirline
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
airlineCode



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOnByVideowall
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOnByQuad
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOff
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitors



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOffByAirline
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
airlineCode



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOffByVideowall
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/SetForcedFailoverOffByQuad
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/ForceDownload
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitors



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/GetCurrentStatus
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitorIndex



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/GetDetailedStatus
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
videowallIndex



	

query
integer
monitorIndex



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/GetMonitorStatuses
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/MonitorFailoverApi/GetQuadsAirlines
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quad



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    NfraFidApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/NfraFid/GetDbLastUpdateTime
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetAllFlights/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetXNextFlightsForAirline/{airlineDetails}/{next}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineDetails



	

path
string
next



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetXNextFlightsOfAirlineXWithCutoff/{airlineDetails}/{next}/{cutoffHour}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineDetails



	

path
string
next



	

path
integer
cutoffHour



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetFlight/{airlineDetails}/{flightNumber}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          airlineDetails



	

path
string
flightNumber



	

path
string
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetAllFlightsForTheNextXHours/{xHours}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xHours



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetAllFlightsForTheNextXDays/{xDays}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xDays



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetAllFlightsForTheLastXHours/{xHours}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xHours



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetAllFlightsForTheLastXDays/{xDays}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          xDays



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/NfraFid/GetXFlightsForAirlineFromUntilOffset/{minutesBefore}/{minutesAfter}/{airlineDetails}/{count}/{adInfo}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          minutesBefore



	

path
integer
minutesAfter



	

path
integer
airlineDetails



	

path
string
count



	

path
integer
adInfo

  



       Departing  


       Arriving  


       All  


       Unknown  


  



path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    OrchestrationModuleApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/Counters
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/CounterAssignments
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/TimingRules
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/GetActualPins
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/SendTestEmail
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/RunPinsValidation/{quadName}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/OrchestrationModule/RunPinsCompare/{quadName}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/OrchestrationModule/RunPinsDelta/{quadName}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

path
string

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/OrchestrationModule/GetZoneManagerChanges
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Inline Model {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          updated

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Inline Model {}
  

  
    {}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/OrchestrationModule/ZoneManagerSaveChanges
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          updated

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Inline Model {}
  

  
    {}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/SetForcedOnAirline
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          quadName



	

query
string
airlineCode



	

query
string
isForcedOn

  



       true  


       false  


  



query
boolean

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/GetTicketCountersRaw
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/GetTicketCountersList
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/OrchestrationModule/SetTicketCounters
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          ticketCounters

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Inline Model {}
  

  
    {}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/OrchestrationModule/SetCheckinData
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          checkinSubmissionModel

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    CheckinSubmissionModel {ForcedOnAirlines (object, optional),TicketCounters (object, optional)}
  

  
    {
  &quot;ForcedOnAirlines&quot;: {},
  &quot;TicketCounters&quot;: {}
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/GetFullAirportUnitMappingJson
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/GetScheduledViewTimingRules
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModule/GetNextExecutionTimeMillis
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/OrchestrationModuleApi/GetForcedOnStatuses
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    SecurityModuleApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          post
          
          
          /api/SecurityModule/Register
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          credentials

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Credentials {Username (string, optional),Password (string, optional),Token (string, optional)}
  

  
    {
  &quot;Username&quot;: &quot;string&quot;,
  &quot;Password&quot;: &quot;string&quot;,
  &quot;Token&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/SecurityModule/RemoveUser
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          credentials

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Credentials {Username (string, optional),Password (string, optional),Token (string, optional)}
  

  
    {
  &quot;Username&quot;: &quot;string&quot;,
  &quot;Password&quot;: &quot;string&quot;,
  &quot;Token&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/SecurityModule/GetUsers
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/SecurityModule/Login
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          credentials

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Credentials {Username (string, optional),Password (string, optional),Token (string, optional)}
  

  
    {
  &quot;Username&quot;: &quot;string&quot;,
  &quot;Password&quot;: &quot;string&quot;,
  &quot;Token&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/SecurityModule/ValidateToken
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          credentials

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    Credentials {Username (string, optional),Password (string, optional),Token (string, optional)}
  

  
    {
  &quot;Username&quot;: &quot;string&quot;,
  &quot;Password&quot;: &quot;string&quot;,
  &quot;Token&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/SecurityModule/ChangePassword
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          changePasswordBindingModel

				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded




	

body


  Model
  Model Schema




  
    ChangePasswordBindingModel {Username (string, optional),PreviousToken (string, optional),CurrentToken (string, optional)}
  

  
    {
  &quot;Username&quot;: &quot;string&quot;,
  &quot;PreviousToken&quot;: &quot;string&quot;,
  &quot;CurrentToken&quot;: &quot;string&quot;
}
    Click to set as parameter value
  




          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    SitaWaitTimeApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /Api/SitaWaitTime/GetSitaWaitTime
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    SystemApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/SystemApi/ThreadCount
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  


  
    WeatherApi 
  
  
    
      Show/Hide
    
    
      
        List Operations
      
    
    
      
        Expand Operations
      
    
  




  
    
      
        
          
          get
          
          
          /api/Weather/GetCurrentWeather/{location}/{selected}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          location



	

path
string
selected

  



       true  


       false  


  



path
boolean

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/Weather/GetForecastWeather/{location}/{numberOfDays}/{selected}
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          location



	

path
string
numberOfDays



	

path
integer
selected

  



       true  


       false  


  



path
boolean

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          get
          
          
          /api/WeatherApi/GetWeatheResult
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          weatherType



	

query
string
location



	

query
string
numberOfDays



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/WeatherApi/AutoCompleteGetWeather
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          weatherType



	

query
string
location



	

query
string
numberOfDays



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  

  
    
      
        
          
          post
          
          
          /api/WeatherApi/LocationSearch
          
        
        
          
          
          
        
      
      

        
          Response Class (Status 200)
            OK

          

  Model
  Model Schema




  
    Object {}
  

  
    {}
    
  



          
          Response Content Type

  application/json
  text/json
  application/xml
  text/xml





        
          
          Parameters
          
          
            
            Parameter
            Value
            Description
            Parameter Type
            Data Type
            
          
          

          weatherType



	

query
string
location



	

query
string
numberOfDays



	

query
integer

          
          
            
            Hide Response
            
          
        
        
          Curl
          
          Request URL
          
          Response Body
          
          Response Code
          
          Response Headers
          
        
      
    
  



  
    [ base url: 
  , api version: v1
    ]
    
    
    
    




/html[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]</value>
   </webElementProperties>
</WebElementEntity>
